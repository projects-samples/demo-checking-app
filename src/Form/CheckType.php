<?php

namespace App\Form;

use App\Entity\Check;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CheckType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('payment_date', DateTimeType::class, [
                'label' => 'date of payment'
            ])
            ->add('number', NumberType::class, [
                'required' => true,
                'label' => 'account number'
            ])
            ->add('amount', NumberType::class, [
                'required' => true,
                'label' => 'cash amount (€)'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'reason of the transfer'
            ])
            ->add('bank', null, [
                'label' => 'origin of the check',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Check::class,
        ]);
    }
}
