<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Bank;
use App\Entity\Check;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

final class CheckAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            // ->add('id')
            ->add('payment_date')
            ->add('number')
            ->add('amount')
            // ->add('description')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            // ->add('id')
            // ->add('payment_date')
            ->add('number')
            ->add('amount')
            ->add('description')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            // ->add('id')
            ->with("Main Informations", ['class' => 'col-md-5'])
            ->add('number')
            ->add('amount')
            ->add('description')
            ->end()
            ->with("Metadata", ['class' => 'col-md-7'])
            ->add('payment_date')
            ->add('bank', ModelType::class, [
                'class' => Bank::class,
            ])
            ->end()
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('payment_date')
            ->add('number')
            ->add('amount')
            ->add('description')
            ;
    }

    public function toString($object)
    {
        return $object instanceof Check ? $object->getAmount() . " € from " . $object->getBank()->getShortName() : 'Check';
    }
}
