<?php

namespace App\DataFixtures;

use App\Entity\Bank;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class BankFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $bank_names = [
            [
                "name" => "Bank Negara Indonesia",
                "short_name" => "BNI",
                "description" => "PT Bank Negara Indonesia Tbk or Bank Negara Indonesia, is an Indonesian state-owned bank. It has branches primarily in Indonesia, but it can also found in Seoul, Singapore, Hong Kong, Tokyo, London and New York. It had 1000 branches and over 9 million customers in 2006." . $faker->paragraph(10, false)
            ],
            [
                "name" => "Bank Of Africa",
                "short_name" => "BOA",
                "description" => "description boa" . $faker->paragraph(30, false)
            ],
            [
                "name" => "BFV Société generale",
                "short_name" => "BFV",
                "description" => "La banque Société Générale Madagasikara est une filiale du groupe bancaire Société Générale. Société Générale  accompagne 31 millions de clients particuliers et entreprises dans le monde et place l'innovation et le digital au cœur de son métier pour améliorer en continu les services qu'elle propose. Présente depuis plus de 20 ans à Madagascar,  Société Générale Madagasikara allie solidité financière et stratégie de croissance durable, avec comme ambition : être LA banque relationnelle de référence sur ses marchés, proche de ses clients, choisie pour la qualité et l’engagement de ses équipes. " . $faker->paragraph(10, false)
            ]
        ];

        for ($i = 0; $i < 3; $i++) {
            $bank_data = $bank_names[$i];
            $bank = (new Bank)
                ->setName($bank_data['name'])
                ->setShortName($bank_data['short_name'])
                ->setDescription($bank_data['description'])
            ;
            $manager->persist($bank);
        }
        $manager->flush();
    }
}
