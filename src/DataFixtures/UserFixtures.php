<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->passwordEncoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $user = (new User)
                ->setEmail($faker->email)
                ->setRoles(['ROLE_USER'])
                ;
            
            $password = 'DemoCheck2k20#';

            if ($user->checkPasswordStrength($password)) {
                $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            } else {
                throw new \Exception("Your password must at least contains 8 characters, have an uppercase letter, a special character and a number", 400);
            }
            
            $manager->persist($user);
        }

        $manager->flush();
    }
}
