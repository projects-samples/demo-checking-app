<?php

namespace App\DataFixtures;

use App\Entity\Check;
use App\Repository\BankRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CheckFixtures extends Fixture
{

    /**
     * @var BankRepository
     */

    private $bankRepository;

    public function __construct(BankRepository $bankRepository)
    {
        $this->bankRepository = $bankRepository;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();
        $banks = $this->bankRepository->findAll();

        for ($i = 0; $i < 30; $i++) {
            $amount = rand(10000, 30000);
            $bank_id = rand(0, count($banks)-1);

            $check = (new Check)
                ->setAmount($amount)
                ->setNumber($faker->bankAccountNumber)
                ->setDescription($faker->realText(200))
                ->setBank($banks[$bank_id])
                ->setPaymentDate($faker->dateTimeBetween('-18 years', 'now', 'EAT'))
            ;

            $manager->persist($check);
        }

        $manager->flush();
    }
}
