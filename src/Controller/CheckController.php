<?php

namespace App\Controller;

use App\Entity\Check;
use App\Form\CheckType;
use App\Repository\BankRepository;
use App\Repository\CheckRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/check")
 */
class CheckController extends AbstractController
{

    /**
     * @var CheckRepository
     */
    private $checkRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var BankRepository
     */
    private $bankRepository;

    public function __construct(CheckRepository $checkRepository, EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator, BankRepository $bankRepository)
    {
        $this->checkRepository = $checkRepository;
        $this->em = $em;
        $this->urlGenerator = $urlGenerator;
        $this->bankRepository = $bankRepository;
    }

    /**
     * @Route("/", name="check_list")
     */
    public function index(Request $request): Response
    {
        $offset = max(0, $request->query->getInt('offset', 0));
        $checksPaginator = $this->checkRepository->getCheckPaginator($offset);
        // $checks = $this->checkRepository->findAll();

        return $this->render('check/index.html.twig', [
            'checks' => $checksPaginator,
            'previous' => $offset - CheckRepository::PAGINATOR_PER_PAGE,
            'next' => min(count($checksPaginator), $offset + CheckRepository::PAGINATOR_PER_PAGE)
        ]);
    }

    /**
     * @Route("/{id}", name="check_show", requirements={"id"="\d+"})
     */
    public function show(Request $request, int $id)
    {
        $check = $this->checkRepository->findOneBy(['id' => $id]);
        return $this->render('check/single.html.twig', [
            'check' => $check
        ]);
    }

    /**
     * @Route("/edit/{id}", name="check_edit", requirements={"id"="\d+"}, methods={"GET", "POST"})
     */
    public function edit(Request $request, int $id)
    {
        $check = $this->checkRepository->findOneBy(['id' => $id]);

        $checkForm = $this->createForm(CheckType::class, $check);
        $checkForm->handleRequest($request);

        if ($checkForm->isSubmitted()&& $checkForm->isValid()) {
            $this->em->persist($check);
            $this->em->flush();

            return $this->redirect($this->urlGenerator->generate('check_show', ['id' => $id]), 302);
        }

        return $this->render('check/edit.html.twig', [
            'checkForm' => $checkForm->createView()
        ]);
    }

    /**
     * @Route("/new", name="check_new", methods={"GET", "POST"})
     */
    public function new(Request $request)
    {
        $check = new Check();
        $checkForm = $this->createForm(CheckType::class, $check);
        $checkForm->handleRequest($request);

        if ($checkForm->isSubmitted()&& $checkForm->isValid()) {
            $this->em->persist($check);
            $this->em->flush();

            return $this->redirect($this->urlGenerator->generate('check_list'), 302);
        }

        $banks = $this->bankRepository->findAll();

        return $this->render('check/new.html.twig', [
            'checkForm' => $checkForm->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="check_delete", requirements={"id"="\d+"})
     */
    public function delete(Request $request, int $id)
    {
        $token_id = 'delete' . $id;

        if ($this->isCsrfTokenValid($token_id, $request->request->get('_token'))) {
            $check = $this->checkRepository->findOneBy(["id" => $id]);
            
            $this->em->remove($check);
            $this->em->flush();

            return $this->redirect($this->urlGenerator->generate('check_list'), 302);
        }

    }
}
