<?php

namespace App\Controller;

use Sonata\AdminBundle\Controller\CRUDController;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class StatsAdminController extends CRUDController
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGeneratorInterface)
    {
        $this->urlGenerator = $urlGeneratorInterface;
    }

    public function listAction()
    {
        return new RedirectResponse($this->urlGenerator->generate("app_stats"));
    }
}