<?php

namespace App\Controller;

use App\Repository\BankRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BankController extends AbstractController
{
    /**
     * @var BankRepository
     */
    private $bankRepo;

    public function __construct(BankRepository $bankRepository)
    {
        $this->bankRepo = $bankRepository;
    }

    /**
     * @Route("/banks", name="bank_list")
     */
    public function index(): Response
    {
        $banks = $this->bankRepo->findAll();
        return $this->render('bank/index.html.twig', [
            'banks' => $banks
        ]);
    }
}
