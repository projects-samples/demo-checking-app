<?php

namespace App\Controller;

use App\Repository\BankRepository;
use App\Repository\CheckRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;

class StatisticsController extends AbstractController
{
    /**
     * @Route("/stats", name="app_stats")
     */
    public function test(BankRepository $bankRepository, CheckRepository $checkRepository)
    {
        $datasets = [];
        $datasets[] = ["Counts", "Checks Per Bank"];

        $banks = $bankRepository->findAll();
        foreach($banks as $bank) {
            $numChecks = $checkRepository->count(['bank' => $bank]);
            $legend = $bank->getName() . " (" . $bank->getShortName() . ")";
            $datasets[] = [$legend, $numChecks];
        }

        $checksPerBankChart = new PieChart();
        $checksPerBankChart->getData()->setArrayToDataTable($datasets);

        $checksPerBankChart->getOptions()->setTitle('Number of checks per bank');
        // $checksPerBankChart->getOptions()->setHeight(500);
        // $checksPerBankChart->getOptions()->setWidth(00);
        $checksPerBankChart->getOptions()->getTitleTextStyle()->setColor('#315A7A');
        $checksPerBankChart->getOptions()->getTitleTextStyle()->setFontName('Roboto');
        $checksPerBankChart->getOptions()->getTitleTextStyle()->setFontSize(16);


        return $this->render('statistics/index.html.twig', [
            'checks_per_bank' => $checksPerBankChart
        ]);
    }
}
