<?php

namespace App\Block\Service;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\Form\Validator\ErrorElement;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Twig\Environment;

class StatsBlockService extends AbstractBlockService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Environment
     */
    private $templating;

    public function __construct(Environment $templating, EntityManagerInterface $em)
    {
        $this->templating = $templating;
        $this->em = $em;
        parent::__construct($this->templating);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Stats Block';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'entity' => 'Add Entity',
            'repository_method' => 'findAll',
            'title' => 'Insert block Title',
            'css_class' => 'bg-blue',
            'icon' => 'fa-users',
            'template' => 'YourBundle:Block:block_stats.html.twig',
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block) {
        $formMapper->add(
            'settings',
            'sonata_type_immutable_array',
            [
                'keys' => [
                    ['entity', 'text', ['required' => false]],
                    ['repository_method', 'text'],
                    ['title', 'text', ['required' => false]],
                    ['css_class', 'text', ['required' => false]],
                    ['icon', 'text', ['required' => false]],
                ]
            ]
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block) {
        $errorElement
            ->with('settings[entity]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[repository_method]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[title]')
                ->assertNotNull(array())
                ->assertNotBlank()
                ->assertMaxLength(array('limit' => 50))
            ->end()
            ->with('settings[css_class]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[icon]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end();
    }
    
    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null
    ): Response {
        $settings = $blockContext->getSettings();
        $entity = $settings['entity'];
        $method = $settings['repository_method'];
        $rows = $this->em->getRepository($entity)->$method();
        return $this->templating->render($blockContext->getTemplate(), [
            'count'     => $rows,
            'block'     => $blockContext->getBlock(),
            'settings'  => $settings,
            ],
        $response);
    }
}