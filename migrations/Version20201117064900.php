<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201117064900 extends AbstractMigration
{
    public function getDescription() : string
    {
        return "Modification de la colonne description de la table Bank";
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__bank AS SELECT id, name, short_name, description FROM bank');
        $this->addSql('DROP TABLE bank');
        $this->addSql('CREATE TABLE bank (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE BINARY, short_name VARCHAR(8) NOT NULL COLLATE BINARY, description CLOB NOT NULL)');
        $this->addSql('INSERT INTO bank (id, name, short_name, description) SELECT id, name, short_name, description FROM __temp__bank');
        $this->addSql('DROP TABLE __temp__bank');
        $this->addSql('DROP INDEX IDX_3C8EAC1311C8FB41');
        $this->addSql('CREATE TEMPORARY TABLE __temp__check AS SELECT id, bank_id, payment_date, number, amount, description FROM "check"');
        $this->addSql('DROP TABLE "check"');
        $this->addSql('CREATE TABLE "check" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, bank_id INTEGER NOT NULL, payment_date DATETIME NOT NULL, number VARCHAR(255) NOT NULL COLLATE BINARY, amount VARCHAR(30) NOT NULL COLLATE BINARY, description VARCHAR(255) NOT NULL COLLATE BINARY, CONSTRAINT FK_3C8EAC1311C8FB41 FOREIGN KEY (bank_id) REFERENCES bank (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO "check" (id, bank_id, payment_date, number, amount, description) SELECT id, bank_id, payment_date, number, amount, description FROM __temp__check');
        $this->addSql('DROP TABLE __temp__check');
        $this->addSql('CREATE INDEX IDX_3C8EAC1311C8FB41 ON "check" (bank_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TEMPORARY TABLE __temp__bank AS SELECT id, name, short_name, description FROM bank');
        $this->addSql('DROP TABLE bank');
        $this->addSql('CREATE TABLE bank (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(8) NOT NULL, description VARCHAR(255) DEFAULT NULL COLLATE BINARY)');
        $this->addSql('INSERT INTO bank (id, name, short_name, description) SELECT id, name, short_name, description FROM __temp__bank');
        $this->addSql('DROP TABLE __temp__bank');
        $this->addSql('DROP INDEX IDX_3C8EAC1311C8FB41');
        $this->addSql('CREATE TEMPORARY TABLE __temp__check AS SELECT id, bank_id, payment_date, number, amount, description FROM "check"');
        $this->addSql('DROP TABLE "check"');
        $this->addSql('CREATE TABLE "check" (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, bank_id INTEGER NOT NULL, payment_date DATETIME NOT NULL, number VARCHAR(255) NOT NULL, amount VARCHAR(30) NOT NULL, description VARCHAR(255) NOT NULL)');
        $this->addSql('INSERT INTO "check" (id, bank_id, payment_date, number, amount, description) SELECT id, bank_id, payment_date, number, amount, description FROM __temp__check');
        $this->addSql('DROP TABLE __temp__check');
        $this->addSql('CREATE INDEX IDX_3C8EAC1311C8FB41 ON "check" (bank_id)');
    }
}
