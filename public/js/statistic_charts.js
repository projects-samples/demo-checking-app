let checks = document.querySelector('#stats-checks_per_bank')
let checksGraph = new Chart(checks, {
    type: "pie",
    data: {
        labels: {{ banks}},
        datasets: [{
            label: "Num of checks per Bank",
            data: {{ checks_per_bank }},
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
            ],
        }],
        
    }
})